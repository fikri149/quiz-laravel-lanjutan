<?php


// Route::group([

//   'middleware' => 'api',
//   'prefix' => 'auth',
//   'namespace' => 'Auth'

// ], function ($router) {

//   Route::post('register', 'RegisterController');
//   Route::post('login', 'LoginController');
//   Route::post('logout', 'LogoutController');
// });


Route::namespace('Auth')->middleware('api')->group(function () {
  Route::post('register', 'RegisterController')->name('register');
  Route::post('login', 'LoginController')->name('login');
  Route::post('logout', 'LogoutController')->name('logout');
});

Route::namespace('Api')->middleware('auth:api')->group(function () {
  Route::get('/', 'TodoController@index');
  Route::post('/', 'TodoController@store');
  Route::post('/change-done-status/{$id}', 'TodoController@changeDoneStatus');
  Route::post('/delete/{$id}', 'TodoController@delete');
});
